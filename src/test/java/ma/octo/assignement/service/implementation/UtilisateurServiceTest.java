package ma.octo.assignement.service.implementation;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.implementation.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class UtilisateurServiceTest {

    @InjectMocks
    private UtilisateurServiceImpl utilisateurService;

    @Mock
    private UtilisateurRepository utilisateurRepository;

    @Test
    void shouldGetAllComptes() {
        List<Utilisateur> utilisateurs = new ArrayList<>();
        List<UtilisateurDto> utilisateursDto = new ArrayList<>();

        
        utilisateurs.add(new Utilisateur());
        utilisateurs.add(new Utilisateur());
        
        UtilisateurMapper utilsmapper = new UtilisateurMapper();
        
        
        
        given(utilisateurRepository.findAll()).willReturn(utilisateurs);
        utilisateurs.forEach(util -> {

        	utilisateursDto.add(utilsmapper.toDto(util));

        });
        assertEquals(utilisateursDto, utilisateurService.findAll());

        verify(utilisateurRepository).findAll();
    }

}
