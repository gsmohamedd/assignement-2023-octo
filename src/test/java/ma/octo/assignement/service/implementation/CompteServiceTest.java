package ma.octo.assignement.service.implementation;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.implementation.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class CompteServiceTest {

    @InjectMocks
    private CompteServiceImpl compteService;

    @Mock
    private CompteRepository compteRepository;

	
	  @Test void shouldGetCompteByNrCompte() { 
	  
	  Compte compte = new Compte();
	  
	  CompteMapper compteMapper = new CompteMapper();
	  
	  compte.setNumeroCompte("010000A000001000");
	  
	  given(compteRepository.findByNumeroCompte(anyString())).willReturn(compte);
	  
	  assertEquals(compteMapper.toDto(compte),compteService.findByNumeroCompte(compte.getNumeroCompte()));
	  
	  verify(compteRepository,times(1)).findByNumeroCompte(compte.getNumeroCompte()); }
	 

    @Test
    void shouldGetAllComptes() {
        List<Compte> comptes = new ArrayList<>();
        
        List<CompteDto> comptesDto = new ArrayList<>();

        CompteMapper comptemapper = new CompteMapper();
        
        comptes.add(new Compte());
        comptes.add(new Compte());

        given(compteRepository.findAll()).willReturn(comptes);
        comptes.forEach(compte -> {

        	comptesDto.add(comptemapper.toDto(compte));

        });
        assertEquals(comptesDto, compteService.findAll());

        verify(compteRepository).findAll();
    }

}
