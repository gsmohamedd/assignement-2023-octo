package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.util.Gender;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class UtilisateurRepositoryTest {


    @Autowired
    private TransferRepository transferRepository;


    @Autowired
    private UtilisateurRepository utilisateurRepository;


    @AfterEach
    void treatDown(){
        utilisateurRepository.deleteAll();
    }




    @Test
    public void shouldCheckExistingUserByUsername() throws ParseException {


        //given
        String username = "user_1";

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user_1");
        utilisateur1.setLastname("MED");
        utilisateur1.setFirstname("MED");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1099");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);
        utilisateurRepository.save(utilisateur1);

        //when
        Utilisateur foundUtilisateur =  utilisateurRepository.findByUsername(username);


        //then
        assertSame(foundUtilisateur, utilisateur1);
    }




    @Test
    public void shouldCheckNonExistingUserByUsername() throws ParseException {

        //given
        String username = "fake_username";

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user_1");
        utilisateur1.setLastname("MED");
        utilisateur1.setFirstname("MED");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1099");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);
        utilisateurRepository.save(utilisateur1);

        //when
        Utilisateur foundUtilisateur =  utilisateurRepository.findByUsername("username");


        //then
        assertNull(foundUtilisateur);
    }


    @Test
    public void CheckSavingInDataBase() throws ParseException {


        //given
        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user_1");
        utilisateur1.setLastname("MED");
        utilisateur1.setFirstname("MED");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1899");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);

        //when
        Utilisateur utilisateur = utilisateurRepository.save(utilisateur1);



        //then
        assertSame(utilisateur,utilisateur1);



    }

    @Test
    public void checkAllSavedUtilisateur()  {
        ArrayList<Utilisateur> utilisateurs =  (ArrayList<Utilisateur>) utilisateurRepository.findAll();
        assertEquals(utilisateurs.size(),2);
    }


    @Test
    public void checkDeletingExistingUser() throws ParseException {


        //given
        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user_1");
        utilisateur1.setLastname("MED");
        utilisateur1.setFirstname("MED");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1099");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);
        Utilisateur savedUtilisateur =  utilisateurRepository.save(utilisateur1);
        utilisateurRepository.delete(savedUtilisateur);

        //when
        Transfer found = transferRepository.findById(savedUtilisateur.getId()).orElse(null);


        //then
        assertNull(found);
    }

}