package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.MontantNonAutoriserException;
import ma.octo.assignement.exceptions.TransactionException;

public interface IDepositService {
	List<DepositDto> findAll();

    DepositDto save(DepositDto depositDto);

    DepositDto depositArgent( DepositDto depositDto)
            throws CompteNonExistantException, TransactionException, MontantNonAutoriserException;

}
