package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.dto.UtilisateurDto;

public interface IUtilisateurService {
	List<UtilisateurDto> findAll();

    UtilisateurDto save(UtilisateurDto utilisateurDto);

    UtilisateurDto findById(Long id);
}
