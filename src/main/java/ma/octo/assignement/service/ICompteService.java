package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.dto.CompteDto;

public interface ICompteService {
	List<CompteDto> findAll();

    CompteDto findByNumeroCompte(String numeroCompte);

    CompteDto save(CompteDto compte);

    CompteDto findByRib(String rib);
}
