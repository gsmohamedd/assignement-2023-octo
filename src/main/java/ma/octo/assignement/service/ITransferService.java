package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

public interface ITransferService {
	  TransferDto save(TransferDto transferDto);

	  List<TransferDto> findAll();

	  TransferDto createTransaction( TransferDto transferDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException;

}
