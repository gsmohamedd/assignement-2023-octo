package ma.octo.assignement.service.implementation;

import java.math.BigDecimal;

import ma.octo.assignement.dto.CompteDto;

public class Utils {
	public static final String EMPTY_STRING = "";


    public static boolean isMontantEmpty(BigDecimal montant){

        return montant.equals(null) || montant.intValue()==0;
    }

    public static boolean isCompteDtoNull(CompteDto compteDto){

        return compteDto == null;
    }


    public static boolean isMontantDtoLessThenTeen(BigDecimal montant){

        return montant.intValue()<10;

    }

    public static BigDecimal updateSoldEmetteur(BigDecimal soldEmetteur , BigDecimal SoldTansfere){

        return BigDecimal.valueOf(soldEmetteur.intValue()-SoldTansfere.intValue());
    }


    public static BigDecimal updateSoldBinificaire(BigDecimal soldEmetteur , BigDecimal SoldTansfere){

        return BigDecimal.valueOf(soldEmetteur.intValue()+SoldTansfere.intValue());
    }
    
    
    public static boolean isLess(BigDecimal a, BigDecimal b) {
    	return a.compareTo(b) < 0;
    }
}
