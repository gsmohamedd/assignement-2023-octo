package ma.octo.assignement.service.implementation;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.ICompteService;

@Service
@Transactional
@AllArgsConstructor

public class CompteServiceImpl implements ICompteService {


    
    private CompteRepository compteRepository;

    private final CompteMapper compteMapper = new CompteMapper();



    
    public List<CompteDto> findAll() {
        List<Compte> compteList = compteRepository.findAll();

        if (!CollectionUtils.isEmpty(compteList)){
            List<CompteDto> compDtos = new ArrayList<>();
            compteList.forEach(compte -> {

                compDtos.add(compteMapper.toDto(compte));

            });

            return compDtos;
        }

        return null;
    }

    @Override
    public CompteDto findByNumeroCompte(String numeroCompte) {
         Compte compte = compteRepository.findByNumeroCompte(numeroCompte);
        if (compte!=null){
            return compteMapper.toDto(compte);
        }

        return null;
    }

    @Override
    public CompteDto save(CompteDto compteDto) {

        if (compteDto!=null){
            Compte compte = compteMapper.toModel(compteDto);
            Compte savedCompte =  compteRepository.save(compte);
            return  compteMapper.toDto(savedCompte);

        }
        return null;


    }

    @Override
    public CompteDto findByRib(String rib) {

        Compte compte = compteRepository.findByRib(rib);
        if (compte!=null){
            return compteMapper.toDto(compte);
        }
        return null;
    }
}
