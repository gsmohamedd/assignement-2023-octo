package ma.octo.assignement.service.implementation;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.MontantNonAutoriserException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.ICompteService;
import ma.octo.assignement.service.IDepositService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
@Service
@Transactional
@Slf4j
@AllArgsConstructor

public class DepositImpl implements IDepositService {


    private DepositRepository DepositRepository;

    private ICompteService compteService;

    private IAuditService auditService;



    @Override
    public List<DepositDto> findAll() {
        List<Deposit> DepositList = DepositRepository.findAll();

        if (!CollectionUtils.isEmpty(DepositList)){
            List<DepositDto> DepositDtos = new ArrayList<>();
            DepositMapper DepositMapper = new DepositMapper();
            DepositList.forEach(Deposit -> {

                DepositDtos.add( DepositMapper.toDto(Deposit));


            });

            return DepositDtos;
        }

        return null;
    }


    @Override
    public DepositDto save(DepositDto depositAgentDto) {

        if (depositAgentDto!=null){
            DepositMapper DepositMapper = new DepositMapper();
            Deposit Deposit = DepositMapper.toModel(depositAgentDto);
            Deposit savedDeposit =  DepositRepository.save(Deposit);
            return  DepositMapper.toDto(savedDeposit);

        }
        return null;
    }

    @Override
    public DepositDto depositArgent(DepositDto DepositDto) throws CompteNonExistantException, TransactionException, MontantNonAutoriserException {
        CompteDto compteDto = compteService.findByRib(DepositDto.getCompteBeneficiaire().getRib());

        if (compteDto == null) {
            log.debug("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }



        if (Utils.isMontantEmpty(DepositDto.getMontant())) {

            log.debug("Montant invalid");
            throw new TransactionException("Montant invalid");

        } else if (DepositDto.getMontant().intValue() > AppInfo.MONTANT_MAXIMAL) {
            log.debug("Le montant maximal est 10000");
            throw new MontantNonAutoriserException("Le montant maximal est 10000");
        }
        if (DepositDto.getMotifDeposit() == null || DepositDto.getMotifDeposit().isBlank()) {
            log.debug("Votre MotifDeposit est vide");
            throw new TransactionException("Votre MotifDeposit est vide");
        }

        if(DepositDto.getNomPrenomEmetteur() == null || DepositDto.getNomPrenomEmetteur().isBlank()) {
            log.debug("nom vide");
            throw new TransactionException("nom vide");
        }


        compteDto.setSolde(new BigDecimal(compteDto.getSolde().intValue() + DepositDto.getMontant().intValue()));
        compteService.save(compteDto);

        
        DepositDto saved = this.save(DepositDto);

        auditService.auditDeposit("Deposit de M/Mme. " +DepositDto.getNomPrenomEmetteur()
                + " , votre RIB est : " + DepositDto.getCompteBeneficiaire().getRib()
                + " avec un montant de  " + DepositDto.getMontant().toString());


        return saved;
    }


}
