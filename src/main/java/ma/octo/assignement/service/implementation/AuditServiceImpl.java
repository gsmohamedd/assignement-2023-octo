package ma.octo.assignement.service.implementation;


import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.AuditDeposit;
import ma.octo.assignement.domain.AuditTransfer;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditDepositRepository;
import ma.octo.assignement.repository.AuditTransferRepository;
import ma.octo.assignement.service.IAuditService;


@Service
@Slf4j
@AllArgsConstructor
@Transactional
public class AuditServiceImpl implements IAuditService{
	
	private AuditTransferRepository auditTransferRepository;
    private AuditDepositRepository auditDepositRepository;

	public void auditTransfer(String message) {
		
		log.info("Audit de l'événement {}", EventType.TRANSFER);

        AuditTransfer audit = new AuditTransfer();
        audit.setEventType(EventType.TRANSFER);
        audit.setMessage(message);
        auditTransferRepository.save(audit);		
	}

	public void auditDeposit(String message) {
		log.info("Audit de l'événement {}", EventType.DEPOSIT);

        AuditDeposit audit = new AuditDeposit();
        audit.setEventType(EventType.DEPOSIT);
        audit.setMessage(message);
        auditDepositRepository.save(audit);
		
	}

}
