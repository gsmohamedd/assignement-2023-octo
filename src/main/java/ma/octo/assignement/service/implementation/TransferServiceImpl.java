package ma.octo.assignement.service.implementation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.ICompteService;
import ma.octo.assignement.service.ITransferService;

@Service
@Transactional
@Slf4j
@AllArgsConstructor

public class TransferServiceImpl implements ITransferService {


    TransferRepository transferRepository;

    ICompteService compteService;

    IAuditService auditService;



    @Override
    public TransferDto save(TransferDto transferDto) {

        if (transferDto!=null){
            TransferMapper transferMapper =  new TransferMapper();
            Transfer transfer = transferMapper.toModel(transferDto);
            Transfer savedTransfer =  transferRepository.save(transfer);
            return transferMapper.toDto(savedTransfer);

        }
        return null;

    }

    @Override
    public List<TransferDto> findAll() {
        List<Transfer> transferList = transferRepository.findAll();

        if (!CollectionUtils.isEmpty(transferList)){
            List<TransferDto> transferDtos = new ArrayList<>();
            TransferMapper transferMapper = new TransferMapper();
            transferList.forEach(transfer -> {

                transferDtos.add( transferMapper.toDto(transfer));


            });

            return transferDtos;
        }

        return null;
    }

    @Override
    public TransferDto createTransaction(TransferDto transferDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        CompteDto compteEmetteurDto = compteService.findByNumeroCompte(transferDto.getCompteEmetteur().getNumeroCompte());
        CompteDto compteBeneficiaireDto = compteService.findByNumeroCompte(transferDto.getCompteBeneficiaire().getNumeroCompte());

        if (Utils.isCompteDtoNull( compteEmetteurDto )) {

            log.debug("Compte Emetteur Non existant");
            throw new CompteNonExistantException("Compte Emetteur Non existant");

        }

        if (Utils.isCompteDtoNull( compteBeneficiaireDto )) {

            log.debug("Compte Beneficiaire Non existant");
            throw new CompteNonExistantException("Compte Beneficiaire Non existant");

        }

        if (Utils.isMontantEmpty( transferDto.getMontantTransfer() )) {

            log.debug("Montant vide");
            throw new TransactionException("Montant vide");

        } else if ( Utils.isLess( transferDto.getMontantTransfer(), new BigDecimal(10) )) {

            log.debug("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");

        } else if (Utils.isLess(new BigDecimal(AppInfo.MONTANT_MAXIMAL), transferDto.getMontantTransfer())) {

            log.debug("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        if (transferDto.getMotifTransfer() == null || transferDto.getMotifTransfer().isBlank()) {
            log.debug("Motif vide");
            throw new TransactionException("Motif vide");

        }

        if (Utils.isLess(compteEmetteurDto.getSolde(), transferDto.getMontantTransfer())) {

            log.debug("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");

        }


        
        compteEmetteurDto.setSolde(Utils.updateSoldEmetteur(compteEmetteurDto.getSolde(),transferDto.getMontantTransfer()));
        
        compteBeneficiaireDto.setSolde(Utils.updateSoldBinificaire(compteEmetteurDto.getSolde(),transferDto.getMontantTransfer()));
        

        compteService.save(compteBeneficiaireDto);
        compteService.save(compteEmetteurDto);
        TransferDto saved = this.save(transferDto);

        auditService.auditTransfer("Transfer depuis " + transferDto.getCompteEmetteur().getNumeroCompte() + " vers " + transferDto
                .getCompteBeneficiaire().getNumeroCompte() + " d'un montant de " + transferDto.getMontantTransfer()
                .toString());

        return  saved;


    }
}
