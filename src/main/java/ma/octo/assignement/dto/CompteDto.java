package ma.octo.assignement.dto;

import java.math.BigDecimal;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompteDto {

	    private Long id;

	    private String numeroCompte;

	    private String rib;

	    private BigDecimal solde;

	    private UtilisateurDto utilisateur;

	}



