package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.*;

@Data
@ToString

public class DepositDto {
	private Long id;

    private BigDecimal montant;


    private Date dateExecution;


    private String nomPrenomEmetteur;


    private CompteDto compteBeneficiaire;


    private String motifDeposit;

    public DepositDto(){

    }

}
