package ma.octo.assignement.dto;

import java.util.Date;

import lombok.*;
import ma.octo.assignement.domain.util.Gender;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UtilisateurDto {
	private Long id;

    private String username;

    private Gender gender;

    private String lastname;

    private String firstname;

    private Date birthdate;

}
