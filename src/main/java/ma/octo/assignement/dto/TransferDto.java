package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransferDto {
	  private Long id;

	  private CompteDto compteEmetteur;

	  private CompteDto compteBeneficiaire;

	  private String motifTransfer;

	  private BigDecimal montantTransfer;

	  private Date dateExecution;


}
