package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;

public class DepositMapper {
	
	public DepositDto toDto(Deposit Deposit) {
        if ( Deposit == null ) {
            return null;
        }

        DepositDto DepositDto = new DepositDto();
        CompteMapper compteMapper = new CompteMapper();
        DepositDto.setId( Deposit.getId() );
        DepositDto.setMontant( Deposit.getMontant() );
        DepositDto.setDateExecution( Deposit.getDateExecution() );
        DepositDto.setNomPrenomEmetteur( Deposit.getNomPrenomEmetteur() );
        DepositDto.setCompteBeneficiaire( compteMapper.toDto( Deposit.getCompteBeneficiaire() ) );
        DepositDto.setMotifDeposit( Deposit.getMotifDeposit() );

        return DepositDto;
    }


    public Deposit toModel(DepositDto DepositDto) {
        if ( DepositDto == null ) {
            return null;
        }

        Deposit Deposit = new Deposit();
        CompteMapper compteMapper = new CompteMapper();
        Deposit.setId( DepositDto.getId() );
        Deposit.setMontant( DepositDto.getMontant() );
        Deposit.setDateExecution( DepositDto.getDateExecution() );
        Deposit.setNomPrenomEmetteur( DepositDto.getNomPrenomEmetteur() );
        Deposit.setCompteBeneficiaire( compteMapper.toModel( DepositDto.getCompteBeneficiaire() ) );
        Deposit.setMotifDeposit( DepositDto.getMotifDeposit() );

        return Deposit;
    }

}
