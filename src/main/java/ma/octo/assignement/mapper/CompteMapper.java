package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;

public class CompteMapper {
	
	public CompteDto toDto(Compte compte) {
        if ( compte == null ) {
            return null;
        }

        CompteDto compteDto = new CompteDto();
        UtilisateurMapper utilisateurMapper = new UtilisateurMapper();
        compteDto.setId( compte.getId() );
        compteDto.setNumeroCompte( compte.getNumeroCompte() );
        compteDto.setRib( compte.getRib() );
        compteDto.setSolde( compte.getSolde() );
        compteDto.setUtilisateur( utilisateurMapper.toDto( compte.getUtilisateur() ) );

        return compteDto;
    }


    public Compte toModel(CompteDto compteDto) {
        if ( compteDto == null ) {
            return null;
        }

        Compte compte = new Compte();
        UtilisateurMapper utilisateurMapper = new UtilisateurMapper();
        compte.setId( compteDto.getId() );
        compte.setNumeroCompte( compteDto.getNumeroCompte() );
        compte.setRib( compteDto.getRib() );
        compte.setSolde( compteDto.getSolde() );
        compte.setUtilisateur( utilisateurMapper.toModel( compteDto.getUtilisateur() ) );

        return compte;
    }

}
