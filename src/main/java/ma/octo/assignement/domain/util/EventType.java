package ma.octo.assignement.domain.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public enum EventType {

  TRANSFER("transfer"),
  DEPOSIT("Deposit d'argent");

  private String type;

}
