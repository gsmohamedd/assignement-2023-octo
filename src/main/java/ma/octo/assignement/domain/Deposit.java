package ma.octo.assignement.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

import lombok.*;


@Entity
@Table(name = "DEPOSIT")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Deposit {
 
		  @Id
		  @GeneratedValue(strategy = GenerationType.IDENTITY)
		  private Long id;

		  @Column(precision = 16, scale = 2, nullable = false)
		  private BigDecimal montant;

		  @Column
		  @Temporal(TemporalType.TIMESTAMP)
		  private Date dateExecution;

		  @Column
		  private String nomPrenomEmetteur;

		  @ManyToOne
		  private Compte compteBeneficiaire;

		  @Column(length = 200)
		  private String motifDeposit;
 
}
