package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.MontantNonAutoriserException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.IDepositService;

@RequestMapping("/api/v1/depositArgent")
@RestController
@Slf4j
public class DepositController {


    @Autowired
    private IDepositService depositService;



    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void depositArgent(@RequestBody DepositDto depositDto)
            throws CompteNonExistantException, TransactionException, MontantNonAutoriserException {
        log.debug("deposit Argent ...");
        depositService.depositArgent(depositDto);

    }



    @GetMapping
    List<DepositDto> findAllDeposit() {

        log.debug("Lister les Deposits");
        return depositService.findAll();
    }



}