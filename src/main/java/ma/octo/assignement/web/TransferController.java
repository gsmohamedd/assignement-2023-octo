package ma.octo.assignement.web;


import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

import ma.octo.assignement.service.ITransferService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

import javax.transaction.Transactional;

@RestController
@Transactional
@RequestMapping(path = "/api/v1/transfer")
@Slf4j
class TransferController {

    @Autowired
    private ITransferService transferService;



    @GetMapping
    List<TransferDto> loadAll() {
        log.debug("Lister les Transfers");
        return transferService.findAll();

    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

            log.debug("create Transaction ...");
            transferService.createTransaction(transferDto);
        
    }





}
