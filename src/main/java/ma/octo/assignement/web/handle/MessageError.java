package ma.octo.assignement.web.handle;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Date;


@Data
public class MessageError {

     Date timestamp;
     HttpStatus status;
     String error;
     String source;


}