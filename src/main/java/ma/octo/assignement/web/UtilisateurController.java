package ma.octo.assignement.web;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.service.IUtilisateurService;

@RestController
@Transactional
@RequestMapping(path = "/api/v1/utilisateurs")
@Slf4j
@AllArgsConstructor
public class UtilisateurController {


    private IUtilisateurService utilisateurService;

    @GetMapping
    List<UtilisateurDto> loadAllUtilisateur() {
        log.debug("list des utilisateurs .. ");
        return utilisateurService.findAll();

    }
}
